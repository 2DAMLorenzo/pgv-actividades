package child;

import java.util.Scanner;

/**
 * Created by thel0w3r on 03/10/2018.
 * All Rights Reserved.
 */
public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println(sc.nextLine().toUpperCase());
    }

}
