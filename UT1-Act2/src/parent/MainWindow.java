package parent;

import parent.util.Window;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

/**
 * Created by thel0w3r on 03/10/2018.
 * All Rights Reserved.
 */
public class MainWindow extends Window {

    private JTextField childPath;
    private JButton openBtn;
    private JFileChooser fileChooser;
    private JLabel infoOutput;
    private JTextArea childOutput;
    private JButton runBtn;

    private File selectedFile;

    public static void main(String[] args) {
        new MainWindow();
    }

    public MainWindow() {
        super("Comunicación bidireccional");
        setupLayout();
        setupEventHandlers();
        this.setVisible(true);
    }

    private void setupLayout() {
        childPath = new JTextField();
        childPath.setEnabled(false);
        positionElement(0, 0, 2, 1, 1, 0, childPath);
        openBtn = new JButton("...");
        positionElement(2, 0, 1, 1, 0, 0, openBtn);
        infoOutput = new JLabel("Child process output:");
        positionElement(0, 1, 3, 1, 1, 0, infoOutput);
        childOutput = new JTextArea();
        positionElement(0, 2, 3, 1, 1, 1, new JScrollPane(childOutput));
        runBtn = new JButton("Run");
        positionElement(0, 3, 3, 1, 1, 0, runBtn);
        fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new FileNameExtensionFilter("Java Applications", "jar"));
        fileChooser.setAcceptAllFileFilterUsed(false);
    }

    private void setupEventHandlers() {
        openBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int returnVal = fileChooser.showOpenDialog(MainWindow.this);
                if(returnVal == JFileChooser.APPROVE_OPTION) {
                    selectedFile = fileChooser.getSelectedFile();
                    childPath.setText(selectedFile.getAbsolutePath());
                }
            }
        });
        runBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(selectedFile != null) {
                    runChild(selectedFile.getAbsolutePath());
                }
            }
        });
    }

    private void runChild(String filepath) {
        try {
            Process p = new ProcessBuilder("java", "-jar", filepath).start();
            BufferedWriter bw = new BufferedWriter(
                    new OutputStreamWriter(p.getOutputStream())
            );

            BufferedReader br = new BufferedReader(
                    new InputStreamReader(p.getInputStream())
            );

            String input = JOptionPane.showInputDialog(MainWindow.this, "The child process asks for input:", "Send input", JOptionPane.PLAIN_MESSAGE);

            if(input != null) {
                bw.write(input);
                bw.newLine();
                bw.flush();

                StringBuffer sb = new StringBuffer();
                String line;
                while((line = br.readLine()) != null) {
                    sb.append(line).append("\r\n");
                }
                childOutput.setText(sb.toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
