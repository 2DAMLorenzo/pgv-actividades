package ut4examen_client.stages;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import ut4examen_client.Communicator;
import ut4examen_client.FXUtils;
import ut4examen_client.LocalFileSystem;

import java.io.File;

public class FTPStage {
    public ListView<String> localFileList;
    public Button uploadButton;
    public Button countButton;
    public Button backButton;
    public TextField currentDirField;
    public Button reloadButton;

    private ObservableList<String> localFiles = FXCollections.observableArrayList();

    public void initialize() {
        currentDirField.setText(LocalFileSystem.getInstance().getCurrentDir());
        localFileList.setItems(localFiles);
        updateList();
    }

    public void onUploadButtonClick(ActionEvent actionEvent) {
        String selectedItem = localFileList.getSelectionModel().getSelectedItem();

        if(selectedItem == null)
            return;

        File selectedFile = new File(new File(LocalFileSystem.getInstance().getCurrentDir()), selectedItem);

        if(!selectedFile.exists())
            return;

        new Thread(() -> {
            if(Communicator.getInstance().upload(selectedFile.getAbsolutePath()))
                Platform.runLater(() -> FXUtils.showAlert(Alert.AlertType.INFORMATION, "File/folder uploaded",
                        "Upload successful", ButtonType.OK));
            else
                Platform.runLater(() -> FXUtils.showAlert(Alert.AlertType.INFORMATION, null,
                        "Error uploading", ButtonType.OK));
        }).start();
    }

    public void onCountButtonClick(ActionEvent actionEvent) {
        new Thread(() -> {
            int count = Communicator.getInstance().getFileCount();
            Platform.runLater(() ->
                    FXUtils.showAlert(Alert.AlertType.INFORMATION, String.valueOf(count),
                            "Number of files and folders", ButtonType.OK));
        }).start();
    }

    public void onBackButtonClick(ActionEvent actionEvent) {
        String parent = new File(LocalFileSystem.getInstance().getCurrentDir())
                .getParent();

        if(parent != null)
            LocalFileSystem.getInstance().setCurrentDir(parent);

        updateList();
    }

    public void onCurrentDirFieldKeyPressed(KeyEvent keyEvent) {
        if(keyEvent.getCode() == KeyCode.ENTER) {
            File selected = new File(currentDirField.getText());
            if(selected.exists() && selected.isDirectory()) {
                LocalFileSystem.getInstance().setCurrentDir(selected.getAbsolutePath());
                updateList();
            } else
                currentDirField.setText(LocalFileSystem.getInstance().getCurrentDir());
        }
    }

    public void onReloadButtonClick(ActionEvent actionEvent) {
        updateList();
    }

    public void onLocalFileListClick(MouseEvent mouseEvent) {
        String selectedItem = localFileList.getSelectionModel().getSelectedItem();

        if(selectedItem == null)
            return;

        if (mouseEvent.getClickCount() == 2) {
            File selected = new File(new File(LocalFileSystem.getInstance().getCurrentDir()), selectedItem);
            if(selected.exists() && selected.isDirectory()) {
                LocalFileSystem.getInstance().setCurrentDir(selected.getAbsolutePath());
                updateList();
            }
        }
    }

    private void updateList() {
        File current = new File(LocalFileSystem.getInstance().getCurrentDir());
        if(current.exists() && current.isDirectory()) {
            localFiles.clear();
            for(File file : current.listFiles())
                localFiles.add(file.getName());
        }
    }
}
