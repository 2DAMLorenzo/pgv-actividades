package ut4examen_client.stages;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import ut4examen_client.Communicator;
import ut4examen_client.FXUtils;
import ut4examen_client.MainApp;

import java.io.IOException;

public class LoginStage {

    public TextField nameField;

    private Communicator comm;

    public void initialize() {
        comm = Communicator.getInstance();
    }

    public void loginAction() {
        boolean login = comm.login(nameField.getText());
        if(!login) {
            FXUtils.showAlert(Alert.AlertType.ERROR, "The username is already taken!", "Error logging in!", ButtonType.OK);
            return;
        }
        System.out.println("Loading stage...");
        Communicator.getInstance().setUsername(nameField.getText().trim());
        try {
            Parent root = FXMLLoader.load(MainApp.class.getResource("./stages/FTPStage.fxml"));
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setTitle("FTP Client");
            stage.setResizable(true);
            stage.setScene(scene);
            stage.show();
            ((Stage)nameField.getScene().getWindow()).close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
