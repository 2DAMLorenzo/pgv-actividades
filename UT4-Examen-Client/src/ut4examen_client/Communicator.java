package ut4examen_client;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.io.IOException;
import java.net.*;

public class Communicator {

    private static Communicator instance;

    private final int PORT = 6667;

    private DatagramSocket socket;
    private InetAddress serverAddress;
    private String username;

    public Communicator() {
        instance = this;
        try {
            socket = new DatagramSocket();
            serverAddress = InetAddress.getByName("192.168.2.255");;
        } catch (SocketException | UnknownHostException e) {
            e.printStackTrace();
        }
    }

    public static Communicator getInstance() {
        return instance;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean login(String username) {
        String dataString = "AUTH:" + username.trim();
        DatagramPacket packet = new DatagramPacket(dataString.getBytes(),
                dataString.getBytes().length, serverAddress, PORT);
        try {
            socket.send(packet);
            String response  = receive();
            System.out.println(response);
            return response.trim().equalsIgnoreCase("AUTH_GRANTED");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean upload(String filepath) {
        String dataString = "UPLOAD:" + filepath;
        DatagramPacket packet = new DatagramPacket(dataString.getBytes(),
                dataString.getBytes().length, serverAddress, PORT);
        try {
            socket.send(packet);
            String response  = receive();
            System.out.println(response);
            return response.trim().startsWith("UPLOAD_SUCCESSFUL");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public int getFileCount() {
        String dataString = "COM:" + username;
        DatagramPacket packet = new DatagramPacket(dataString.getBytes(),
                dataString.getBytes().length, serverAddress, PORT);
        try {
            socket.send(packet);
            String response  = receive();
            System.out.println(response);
            String[] data = response.split(":");
            if(data[0].trim().equalsIgnoreCase("COUNT"))
                return Integer.valueOf(data[1].trim());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void logout() {
        String dataString = "DC:" + username;
        DatagramPacket packet = new DatagramPacket(dataString.getBytes(),
                dataString.getBytes().length, serverAddress, PORT);
        try {
            socket.send(packet);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String receive() throws IOException {
        byte[] data = new byte[2048];
        DatagramPacket packet = new DatagramPacket(data, data.length);
        socket.receive(packet);
        return new String(packet.getData());
    }
}
