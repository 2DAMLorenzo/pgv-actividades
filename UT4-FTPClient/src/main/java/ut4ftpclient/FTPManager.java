package ut4ftpclient;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.io.CopyStreamListener;

import java.io.*;

public class FTPManager {

    private static FTPManager instance;

    private FTPClient ftpClient;

    public FTPManager() {
        instance = this;
        ftpClient = new FTPClient();
        ftpClient.setConnectTimeout(5000);
        ftpClient.setControlEncoding("UTF-8");
    }

    public static FTPManager getInstance() {
        return instance;
    }

    private boolean connect(String host, int port) {
        try {
            ftpClient.connect(host, port);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean authenticate(String host, int port, String username, String password) {
        boolean connection = connect(host, port);

        if(!connection)
            return false;

        try {
            ftpClient.login(username, password);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    public String getCurrentDirectory() {
        try {
            return ftpClient.printWorkingDirectory();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public FTPFile[] getFiles() {
        try {
            return ftpClient.listFiles();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public long getFileSize(String filename) {
        try {
            return ftpClient.mlistFile(filename).getSize();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public boolean changeDirectory(String folder) {
        try {
            return ftpClient.changeWorkingDirectory(folder);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void downloadFile(String remoteFileName, String localPath, CopyStreamListener progressListener) {
        ftpClient.setCopyStreamListener(progressListener);
        new Thread(() -> {
            try {
                FileOutputStream fout = new FileOutputStream(localPath);
                ftpClient.setFileType(FTP.BINARY_FILE_TYPE, FTP.BINARY_FILE_TYPE);
                ftpClient.setFileTransferMode(FTP.BINARY_FILE_TYPE);
                if(ftpClient.retrieveFile(remoteFileName, fout))
                    Platform.runLater(() -> {
                        FXUtils.showAlert(Alert.AlertType.CONFIRMATION, "The file has been downloaded to\n" + localPath,
                                "File download successful!", ButtonType.OK);
                    });
                else
                    Platform.runLater(() -> {
                        FXUtils.showAlert(Alert.AlertType.ERROR, "There was an error trying to download the file to\n" + localPath,
                                "Error downloading the file!", ButtonType.OK);
                    });
                fout.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    public void uploadFile(String remoteFileName, String localPath, CopyStreamListener progressListener, Callback callback) {
        ftpClient.setCopyStreamListener(progressListener);
        System.out.println("Uploading " + localPath + " to " + remoteFileName);
        new Thread(() -> {
            File file = new File(localPath);

            if(!file.exists() || file.isDirectory())
                return;

            try {
                FileInputStream fis = new FileInputStream(file);
                ftpClient.setFileType(FTP.BINARY_FILE_TYPE, FTP.BINARY_FILE_TYPE);
                ftpClient.setFileTransferMode(FTP.BINARY_FILE_TYPE);
                if(ftpClient.storeFile(remoteFileName, fis)) {
                    Platform.runLater(() -> FXUtils.showAlert(Alert.AlertType.CONFIRMATION, "The file has been uploaded!",
                            "File upload successful!", ButtonType.OK));
                    callback.callback();
                }
                else
                    Platform.runLater(() -> {
                        FXUtils.showAlert(Alert.AlertType.ERROR, "There was an error trying to upload the file \n" + localPath,
                                "Error uploading the file!", ButtonType.OK);
                    });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    public void deleteFile(String filename, Callback callback) {
        new Thread(() -> {
            try {
                if(ftpClient.deleteFile(filename)) {
                    Platform.runLater(() -> FXUtils.showAlert(Alert.AlertType.CONFIRMATION, "The file " + filename + " has been deleted!",
                            "File delete successful!", ButtonType.OK));
                    callback.callback();
                } else
                    Platform.runLater(() -> {
                        FXUtils.showAlert(Alert.AlertType.ERROR, "There was an error trying to delete the file \n" + filename,
                                "Error deleting the file!", ButtonType.OK);
                    });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    public interface Callback {
        void callback();
    }
}
