package ut4ftpclient;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class MainApp extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        new LocalFileSystem();
        new FTPManager();
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/stages/ConnectStage.fxml"));
            Scene scene = new Scene(root);
            primaryStage.setScene(scene);
            primaryStage.setTitle("FTP Server connection");
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
