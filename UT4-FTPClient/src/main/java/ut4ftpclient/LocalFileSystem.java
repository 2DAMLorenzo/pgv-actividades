package ut4ftpclient;

import java.io.File;

public class LocalFileSystem {

    private static LocalFileSystem instance;

    private String currentDir;

    public LocalFileSystem() {
        instance = this;
        currentDir = new File(System.getProperty("user.dir")).getAbsolutePath();
    }

    public static LocalFileSystem getInstance() {
        return instance;
    }

    public String getCurrentDir() {
        return currentDir;
    }

    public void setCurrentDir(String newDir) {
        this.currentDir = newDir;
    }

}
