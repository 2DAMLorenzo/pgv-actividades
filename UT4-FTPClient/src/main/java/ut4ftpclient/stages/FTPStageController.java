package ut4ftpclient.stages;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.io.CopyStreamAdapter;
import ut4ftpclient.FTPManager;
import ut4ftpclient.FXUtils;
import ut4ftpclient.LocalFileSystem;

import java.io.File;
import java.util.Arrays;
import java.util.Optional;

public class FTPStageController {
    public TextField localPathField;
    public ListView<String> localList;
    public ListView<String> remoteList;
    public Button localBackButton;
    public TextField remotePathField;
    public Button downloadRemoteButton;
    public Button localReloadButton;
    public Button remoteReloadButton;
    public Button remoteBackButton;
    public Button deleteRemoteButton;
    public Button uploadButton;
    public ProgressBar downloadProgressBar;

    private ObservableList<String> localFiles = FXCollections.observableArrayList();
    private ObservableList<String> remoteFiles = FXCollections.observableArrayList();

    public void initialize() {
        localList.setItems(localFiles);
        remoteList.setItems(remoteFiles);
        loadLocalDirectory();
        loadRemoteDirectory();
    }

    private void loadRemoteDirectory() {
        remoteFiles.clear();
        for(FTPFile remoteFile : FTPManager.getInstance().getFiles()){
            remoteFiles.add(remoteFile.getName());
        }
        remotePathField.setText(FTPManager.getInstance().getCurrentDirectory());
    }

    private void loadLocalDirectory() {
        localFiles.clear();

        File path = new File(LocalFileSystem.getInstance().getCurrentDir());
        if(path.exists() && path.isDirectory()) {
            String[] files = path.list();
            if (files != null) {
                localFiles.addAll(Arrays.asList(files));
            }
        }

        localPathField.setText(path.getAbsolutePath());
    }

    public void onLocalBackButtonClick(ActionEvent actionEvent) {
        String parent = new File(LocalFileSystem.getInstance().getCurrentDir())
                .getParent();
        if(parent != null)
            LocalFileSystem.getInstance().setCurrentDir(parent);
        loadLocalDirectory();
    }

    public void onRemoteBackButtonClick(ActionEvent actionEvent) {
        if(FTPManager.getInstance().changeDirectory(".."))
            loadRemoteDirectory();
    }

    public void onLocalListMouseClick(MouseEvent mouseEvent) {
        String selectedItem = localList.getSelectionModel().getSelectedItem();

        if(selectedItem == null)
            return;

        if (mouseEvent.getClickCount() == 2) {
            File selected = new File(new File(LocalFileSystem.getInstance().getCurrentDir()), selectedItem);
            if(selected.exists() && selected.isDirectory()) {
                LocalFileSystem.getInstance().setCurrentDir(selected.getAbsolutePath());
                loadLocalDirectory();
            }
        }
    }

    public void onRemoteListMouseClick(MouseEvent mouseEvent) {
        String selected = remoteList.getSelectionModel().getSelectedItem();

        if(selected == null)
            return;

        if (mouseEvent.getClickCount() == 2) {
            if(FTPManager.getInstance().changeDirectory(selected))
                loadRemoteDirectory();
        }
    }

    public void onLocalPathFieldKeyPressed(KeyEvent keyEvent) {
        if(keyEvent.getCode() == KeyCode.ENTER) {
            File selected = new File(localPathField.getText());
            if(selected.exists()) {
                LocalFileSystem.getInstance().setCurrentDir(selected.getAbsolutePath());
                loadLocalDirectory();
            }
        }
    }

    public void onRemotePathFieldKeyPressed(KeyEvent keyEvent) {
        if(keyEvent.getCode() == KeyCode.ENTER) {
            if(FTPManager.getInstance().changeDirectory(remotePathField.getText()))
                loadRemoteDirectory();
        }
    }

    public void onDownloadRemoteClick(ActionEvent actionEvent) {
        String remoteFileName = remoteList.getSelectionModel().getSelectedItem();

        if(remoteFileName == null)
            return;

        File localFileName = new File(LocalFileSystem.getInstance().getCurrentDir(), remoteFileName);
        System.out.println("downloading to: " + localFileName.getAbsolutePath());
        long remoteSize = FTPManager.getInstance().getFileSize(remoteFileName);
        CopyStreamAdapter streamListener = new CopyStreamAdapter() {
            @Override
            public void bytesTransferred(long totalBytesTransferred, int bytesTransferred, long streamSize) {
                float percent = ((float) totalBytesTransferred / (float) remoteSize);
                downloadProgressBar.setProgress(percent);
                if(percent >= 1.0f)
                    downloadProgressBar.setProgress(0);
            }
        };

        if(!localFileName.exists()) {
            FTPManager.getInstance().downloadFile(remoteFileName, localFileName.getAbsolutePath(), streamListener);
            loadLocalDirectory();
        }
    }

    public void onLocalReloadButtonClick(ActionEvent actionEvent) {
        loadLocalDirectory();
    }

    public void onRemoteReloadButtonClick(ActionEvent actionEvent) {
        loadRemoteDirectory();
    }

    public void onDeleteRemoteButtonClick(ActionEvent actionEvent) {
        String selected = remoteList.getSelectionModel().getSelectedItem();

        if(selected == null)
            return;

        Optional res = FXUtils.showAlert(Alert.AlertType.WARNING, "Are you sure you want to delete the file " + selected,
                "You're about to delete a file!", ButtonType.YES, ButtonType.NO);

        if(res.isPresent() && res.get() == ButtonType.YES) {
            FTPManager.getInstance().deleteFile(selected, () -> Platform.runLater(this::loadRemoteDirectory));
        }
    }

    public void onUploadButtonClick(ActionEvent actionEvent) {
        String selected = localList.getSelectionModel().getSelectedItem();

        if(selected == null)
            return;

        File localFileName = new File(LocalFileSystem.getInstance().getCurrentDir(), selected);
        CopyStreamAdapter streamListener = new CopyStreamAdapter() {
            @Override
            public void bytesTransferred(long totalBytesTransferred, int bytesTransferred, long streamSize) {
                float percent = ((float) totalBytesTransferred / (float) localFileName.length());
                downloadProgressBar.setProgress(percent);
                if(percent >= 1.0f)
                    downloadProgressBar.setProgress(0);
            }
        };

        FTPManager.getInstance().uploadFile(selected, localFileName.getAbsolutePath(), streamListener, () -> Platform.runLater(this::loadRemoteDirectory));
    }
}
