package ut4ftpclient.stages;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import ut4ftpclient.FTPManager;
import ut4ftpclient.FXUtils;
import ut4ftpclient.MainApp;

import java.io.IOException;

public class ConnectStageController {
    public TextField hostField;
    public TextField portField;
    public TextField usernameField;
    public PasswordField passwordField;
    public Button closeButton;
    public Button connectButton;

    public void initialize() {
        hostField.setText("192.168.1.102");
        portField.setText("21");
        usernameField.setText("admin");
        passwordField.setText("admin");
    }

    public void onCloseButtonAction(ActionEvent actionEvent) {
        Platform.exit();
    }

    public void onConnectButtonAction(ActionEvent actionEvent) {
        new Thread(() -> {
            boolean connection = FTPManager.getInstance().authenticate(hostField.getText(), Integer.valueOf(portField.getText()),
                    usernameField.getText(), passwordField.getText());

            if(!connection) {
                Platform.runLater(() -> {
                    FXUtils.showAlert(Alert.AlertType.ERROR, "An error has occurred while connecting to the FTP Server!",
                            "Could not connect to the FTP Server!", ButtonType.OK);
                });
                return;
            }

            Platform.runLater(() -> {
                try {
                    FXMLLoader loader = new FXMLLoader(MainApp.class.getResource("/stages/FTPStage.fxml"));
                    Parent root = loader.load();
                    Scene scene = new Scene(root);
                    Stage stage = new Stage();
                    stage.setTitle("FTP Client on " + hostField.getText());
                    stage.setResizable(true);
                    stage.setScene(scene);
                    stage.show();
                    ((Stage)hostField.getScene().getWindow()).close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }).start();
    }
}