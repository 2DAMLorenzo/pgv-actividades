package ut3chatclient.stages;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import ut3chatclient.ChatSystem;
import ut3chatclient.FXUtils;
import ut3chatclient.MainApp;

import java.io.IOException;

public class LoginStage {

    public TextField nameField;

    public void loginAction() {
        boolean login = ChatSystem.getInstance().login(nameField.getText());
        if(!login) {
            FXUtils.showAlert(Alert.AlertType.ERROR, "The username is already taken!", "Error logging in!", ButtonType.OK);
            return;
        }
        try {
            FXMLLoader loader = new FXMLLoader(MainApp.class.getResource("stages/ChatStage.fxml"));
            Parent root = loader.load();
            ChatStage controller = loader.getController();
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setTitle("Chat");
            stage.setResizable(true);
            stage.setScene(scene);
            stage.setOnHidden(e -> controller.shutdown());
            stage.show();
            ((Stage)nameField.getScene().getWindow()).close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
