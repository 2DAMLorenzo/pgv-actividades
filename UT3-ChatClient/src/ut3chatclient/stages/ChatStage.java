package ut3chatclient.stages;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import ut3chatclient.ChatSystem;

import java.io.*;
import java.net.Socket;
import java.net.SocketException;

public class ChatStage {

    public ListView<String> messageList;
    public Button sendBtn;
    public TextArea messageArea;

    private Thread messageThread;
    private ObservableList<String> messages = FXCollections.observableArrayList();

    public void initialize() {
        messageList.setItems(messages);
        messageThread = new Thread(() -> {
            try {
                Socket sock = ChatSystem.getInstance().getSocket();
                BufferedReader br = new BufferedReader(new InputStreamReader(sock.getInputStream()));
                while(true) {
                    String data = br.readLine();
                    switch(data.split(":")[0].trim()) {
                        case "CHAT_MSG":
                            String user = data.split(":")[1].trim();
                            String message = data.split(":")[2].trim()
                                    .replaceAll("%NL%", "\n")
                                    .replaceAll("%CL%", ":");
                            Platform.runLater(() -> {
                                messages.add(user + ": " + message);
                            });
                            break;
                        case "CLIENT_JOIN":
                            Platform.runLater(() -> {
                                messages.add(data.split(":")[1].trim() + " has joined the chat!");
                            });
                            break;
                        case "CLIENT_DC":
                            Platform.runLater(() -> {
                                messages.add(data.split(":")[1].trim() + " has left the chat!");
                            });
                    }
                }
            } catch (SocketException ignored){
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        messageThread.start();
    }

    public void shutdown() {
        messageThread.interrupt();
    }

    public void onSendBtnAction(ActionEvent actionEvent) {
        try {
            Socket sock = ChatSystem.getInstance().getSocket();
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(sock.getOutputStream()));
            bw.write("MSG:" + messageArea.getText()
                    .replaceAll("\n", "%NL%")
                    .replaceAll(":", "%CL%"));
            bw.newLine();
            bw.flush();
            messageArea.clear();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
