package ut3chatclient;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.io.*;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;

public class ChatSystem {

    private static ChatSystem instance;
    private Socket socket;

    public ChatSystem() {
        instance = this;
        this.socket = new Socket();
        try {
            socket.connect(new InetSocketAddress(InetAddress.getLocalHost().getHostAddress(), 46369));
            if(socket.isConnected())
                System.out.println("Connected to server!");
        } catch (IOException e) {
            Platform.runLater(() -> {
                FXUtils.showAlert(Alert.AlertType.ERROR, "An error has occurred trying to connect to the chat server:\n\n" +
                        e.getMessage(), "Error connecting to server!", ButtonType.OK);
            });
        }
    }

    public static ChatSystem getInstance() {
        return instance;
    }

    public Socket getSocket() {
        return socket;
    }

    public void disconnect() {
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean login(String username) {
        try {
            BufferedReader inStream = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            BufferedWriter outStream = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            outStream.write("AUTH:" + username);
            outStream.newLine();
            outStream.flush();
            String line = inStream.readLine();
            return line.equalsIgnoreCase("LOGIN_GRANTED");
        } catch (IOException ignored) {}
        return false;
    }
}
