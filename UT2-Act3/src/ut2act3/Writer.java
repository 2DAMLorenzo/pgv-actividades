package ut2act3;

/**
 * Created by TheL0w3R on 28/11/2018.
 * All Rights Reserved.
 */
public class Writer extends Thread {

    @Override
    public void run() {
        while(true) {
            try {
                Thread.sleep((long)(10000D * Math.random()));
                String value = ResourceManager.getInstance().write();
                System.out.println(Thread.currentThread().getName() + " has written '" + value + "'");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
