package ut2act3;

import java.io.IOException;

/**
 * Created by TheL0w3R on 28/11/2018.
 * All Rights Reserved.
 */
public class Main {

    public static void main(String[] args) {
        new ResourceManager();
        for(int i = 0; i < 5; i++) {
            Writer w = new Writer();
            w.setName("Writer " + (i + 1));
            w.start();
        }
        for(int i = 0; i < 5; i++) {
            Reader r = new Reader();
            r.setName("Reader " + (i + 1));
            r.start();
        }
    }

}
