package ut2act3;

import java.io.*;

/**
 * Created by TheL0w3R on 28/11/2018.
 * All Rights Reserved.
 */
public class FileManager {

    private File file;

    public FileManager() throws IOException {
        file = new File("./amazing.resource");
        if(!file.exists())
            file.createNewFile();
    }

    public void appendToFile(char c) {
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(file, true));
            bw.append(c);
            bw.flush();
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String readFromFile() {
        String character = null;
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            while((line = br.readLine()) != null) {
                character = line.substring(line.length() - 1);
                return line;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
