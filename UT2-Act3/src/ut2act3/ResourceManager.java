package ut2act3;

import java.io.IOException;
import java.util.Random;
import java.util.concurrent.Semaphore;

/**
 * Created by TheL0w3R on 28/11/2018.
 * All Rights Reserved.
 */
public class ResourceManager {

    private Semaphore mutex;
    private FileManager fileManager;
    private boolean valueToRead = false;
    private int readIndex = 0;

    private static ResourceManager instance;

    public ResourceManager() {
        instance = this;
        mutex = new Semaphore(1);
        try {
            fileManager = new FileManager();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ResourceManager getInstance() {
        return instance;
    }

    public String write() throws InterruptedException {
        char randomChar = (char)(new Random().nextInt(26) + 'a');
        mutex.acquire();
        fileManager.appendToFile(randomChar);
        valueToRead = true;
        mutex.release();
        return String.valueOf(randomChar);
    }

    public String read() throws InterruptedException {
        String res = null;
        mutex.acquire();
        if(valueToRead) {
            res = String.valueOf(fileManager.readFromFile().charAt(readIndex));
            readIndex++;
            valueToRead = false;
        }
        mutex.release();
        return res;
    }

}
