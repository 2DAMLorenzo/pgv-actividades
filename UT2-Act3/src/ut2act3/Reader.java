package ut2act3;

/**
 * Created by TheL0w3R on 28/11/2018.
 * All Rights Reserved.
 */
public class Reader extends Thread {

    @Override
    public void run() {
        while(true) {
            try {
                Thread.sleep((long)(5000D * Math.random()));
                String value = ResourceManager.getInstance().read();
                if(value != null)
                    System.out.println(Thread.currentThread().getName() + " reading '" + value + "'");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
