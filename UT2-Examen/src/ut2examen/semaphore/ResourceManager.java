package ut2examen.semaphore;

import ut2examen.Chair;

import java.util.ArrayList;
import java.util.concurrent.Semaphore;

/**
 * Created by thel0w3r on 2018-11-29.
 * All Rights Reserved.
 */
public class ResourceManager {

    private static ResourceManager instance;

    private Semaphore mutex;
    private ArrayList<Chair> chairs;

    public ResourceManager() {
        instance = this;
        mutex = new Semaphore(1);
        chairs = new ArrayList<>();
        for(int i = 0; i < 4; i++) {
            chairs.add(new Chair());
        }
    }

    public static ResourceManager getInstance() {
        return instance;
    }

    public void sitDown(Player player) {
        try {
            mutex.acquire();
            for(Chair chair : chairs) {
                if(chair.canSit()) {
                    chair.sit(player);
                    System.out.println("El jugador " + player.getName() + " se sentó");
                    mutex.release();
                    return;
                }
            }
            System.out.print("El jugador " + player.getName() + " ha perdido!");
            player.setHasLost(true);
            freeChairs();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        mutex.release();
    }

    public boolean isOnAChair(Player player) {
        try {
            mutex.acquire();
            for(Chair chair : chairs) {
                if(chair.getPlayerSitting() != null && player.getName().equalsIgnoreCase(chair.getPlayerSitting().getName())) {
                    mutex.release();
                    return true;
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        mutex.release();
        return false;
    }

    private void freeChairs() {
        if(chairs.size() == 1) {
            System.out.println("\nLa partida ha acabado! Ganador: " + chairs.get(0).getPlayerSitting().getName());
            chairs.get(0).getPlayerSitting().setHasLost(true);
            chairs.get(0).getPlayerSitting().stop();
            chairs.get(0).free();
        } else {
            System.out.println(" Nueva ronda!");
            for(Chair chair : chairs) {
                chair.free();
            }

            chairs.remove(chairs.size() - 1);
        }
    }
}
