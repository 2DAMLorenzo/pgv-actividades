package ut2examen.semaphore;

import ut2examen.monitor.polling.PollingMonitor;

/**
 * Created by thel0w3r on 2018-11-29.
 * All Rights Reserved.
 */

public class Main {

    public static void main(String[] args) {
        System.out.println("**** VERSIÓN SEMAFOROS ****\n" +
                "   Esta versióin funciona exactamente igual que la de monitores con polling.\n" +
                "   La única diferencia es que en lugar de usar monitores se usa un semáforo MUTEX.\n" +
                "******************************************\n");

        new ResourceManager();
        for(int i = 0; i < 5; i++) {
            new Player("Player " + i).start();
        }
    }
}
