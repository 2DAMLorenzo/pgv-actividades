package ut2examen.semaphore;

import ut2examen.BasePlayer;
import ut2examen.monitor.polling.PollingMonitor;

/**
 * Created by thel0w3r on 2018-11-29.
 * All Rights Reserved.
 */
public class Player extends BasePlayer {

    public Player(String name) {
        super(name);
    }

    @Override
    public void run() {
        while(!hasLost) {
            try {
                sleep((long)(Math.random() * 5000));
                if(!ResourceManager.getInstance().isOnAChair(this))
                    ResourceManager.getInstance().sitDown(this);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
