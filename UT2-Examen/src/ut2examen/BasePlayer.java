package ut2examen;

/**
 * Created by thel0w3r on 2018-11-29.
 * All Rights Reserved.
 */
public class BasePlayer extends Thread {

    protected boolean hasLost;
    protected boolean sitting;

    public BasePlayer(String name) {
        setName(name);
        hasLost = false;
        sitting = false;
    }

    public void setHasLost(boolean hasLost) {
        this.hasLost = hasLost;
    }
}
