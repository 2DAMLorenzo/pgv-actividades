package ut2examen;

/**
 * Created by thel0w3r on 2018-11-29.
 * All Rights Reserved.
 */
public class Chair {

    private boolean isFree;
    private BasePlayer playerSitting;

    public Chair() {
        isFree = true;
    }

    public void sit(BasePlayer player) {
        this.isFree = false;
        this.playerSitting = player;
    }

    public boolean canSit() {
        return isFree;
    }

    public void free() {
        this.isFree = true;
        this.playerSitting = null;
    }

    public BasePlayer getPlayerSitting() {
        return playerSitting;
    }
}
