package ut2examen.monitor.signals;

/**
 * Created by thel0w3r on 2018-11-29.
 * All Rights Reserved.
 */

public class Main {

    public static void main(String[] args) {
        System.out.println("**** VERSIÓN DE MONITORES CON SEÑALES ****\n" +
                "   Esta versión del ejercicio SI hace uso de las señales wait() y notify().\n" +
                "   En este caso, el jugador solo realiza una acción: intentar sentarse,\n" +
                "   es el monitor el responsable de manejar el deseo de sentarse del jugador,\n" +
                "   si hay un asiento libre, lo sienta, la proxima vez que el mismo jugador intente\n" +
                "   sentarse (ya que este no sabe si lo está o no) el monitor lo pondrá en espera\n" +
                "   hasta que se liberen los asientos (comienzo de la siguiente ronda).\n" +
                "   La parte negativa de este planteamiento es que al iniciar una nueva ronda,\n" +
                "   los jugadores en espera se sientan inmediatamente.\n" +
                "******************************************\n");

        new Monitor();
        for(int i = 0; i < 5; i++) {
            new Player("Player " + i).start();
        }
    }
}
