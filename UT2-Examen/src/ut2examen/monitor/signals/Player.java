package ut2examen.monitor.signals;

import ut2examen.BasePlayer;

/**
 * Created by thel0w3r on 2018-11-29.
 * All Rights Reserved.
 */
public class Player extends BasePlayer {

    public Player(String name) {
        super(name);
    }

    @Override
    public void run() {
        while(!hasLost) {
            try {
                sleep((long)(Math.random() * 5000));
                Monitor.getInstance().sitDown(this);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
