package ut2examen.monitor.polling;

/**
 * Created by thel0w3r on 2018-11-29.
 * All Rights Reserved.
 */

public class Main {

    public static void main(String[] args) {
        System.out.println("**** VERSIÓN DE MONITORES CON POLLING ****\n" +
                "   Esta versión del ejercicio no hace uso de las señales wait() y notify().\n" +
                "   En este caso es el jugador quien pregunta si está sentado antes de sentarse,\n" +
                "   en caso de no estarlo, se sienta, después de su tiempo de espera, vuelve a preguntar,\n" +
                "   si está sentado, sigue haciendo la misma pregunta hasta que la respuesta del\n" +
                "   Monitor sea un NO.\n" +
                "******************************************\n");

        new PollingMonitor();
        for(int i = 0; i < 5; i++) {
            new Player("Player " + i).start();
        }
    }
}
