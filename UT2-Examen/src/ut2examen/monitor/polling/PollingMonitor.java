package ut2examen.monitor.polling;

import ut2examen.Chair;

import java.util.ArrayList;

/**
 * Created by thel0w3r on 2018-11-29.
 * All Rights Reserved.
 */
public class PollingMonitor {

    private static PollingMonitor instance;

    private ArrayList<Chair> chairs;

    public PollingMonitor() {
        instance = this;
        chairs = new ArrayList<>();
        for(int i = 0; i < 4; i++) {
            chairs.add(new Chair());
        }
    }

    public static PollingMonitor getInstance() {
        return instance;
    }

    public synchronized void sitDown(Player player) {
        for(Chair chair : chairs) {
            if(chair.canSit()) {
                chair.sit(player);
                System.out.println("El jugador " + player.getName() + " se sentó");
                return;
            }
        }
        System.out.print("El jugador " + player.getName() + " ha perdido!");
        player.setHasLost(true);
        freeChairs();
    }

    public synchronized boolean isOnAChair(Player player) {
        for(Chair chair : chairs) {
            if(chair.getPlayerSitting() != null && player.getName().equalsIgnoreCase(chair.getPlayerSitting().getName())) {
                return true;
            }
        }
        return false;
    }

    private synchronized void freeChairs() {
        if(chairs.size() == 1) {
            System.out.println("\nLa partida ha acabado! Ganador: " + chairs.get(0).getPlayerSitting().getName());
            chairs.get(0).getPlayerSitting().setHasLost(true);
            chairs.get(0).getPlayerSitting().stop();
            chairs.get(0).free();
        } else {
            System.out.println(" Nueva ronda!");
            for(Chair chair : chairs) {
                chair.free();
            }

            chairs.remove(chairs.size() - 1);
        }
    }
}
