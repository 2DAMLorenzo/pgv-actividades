package ut3multicastchat;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.io.*;
import java.net.*;

public class ChatSystem {

    private static ChatSystem instance;
    private MulticastSocket socket;
    private InetAddress group;
    private String username;

    public ChatSystem() {
        instance = this;
        try {
            this.socket = new MulticastSocket(46369);
            group = InetAddress.getByName("225.1.1.1");
            socket.joinGroup(group);
            System.out.println("Connected to server!");
        } catch (IOException e) {
            Platform.runLater(() -> {
                FXUtils.showAlert(Alert.AlertType.ERROR, "An error has occurred trying to connect to the chat server:\n\n" +
                        e.getMessage(), "Error connecting to server!", ButtonType.OK);
            });
        }
    }

    public static ChatSystem getInstance() {
        return instance;
    }

    public MulticastSocket getSocket() {
        return socket;
    }

    public void setUsername(String username) {
        this.username = username;
        sendPacket("CLIENT_JOIN:" + username);
    }

    public String getUsername() {
        return username;
    }

    public void sendMessage(String message) {
        String data = "CHAT_MSG:" + username + ":" + message;
        sendPacket(data);
    }

    public void sendPacket(String data) {
        try {
            DatagramPacket paquete=new DatagramPacket(data.getBytes(), data.length(), group, 46369);
            socket.send(paquete);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void disconnect() {
        sendPacket("CLIENT_DC:" + username);
        socket.close();
    }

}
