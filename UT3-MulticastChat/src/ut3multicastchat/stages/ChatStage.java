package ut3multicastchat.stages;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import ut3multicastchat.ChatSystem;

import java.io.IOException;
import java.net.DatagramPacket;

public class ChatStage {
    public ListView<String> messageList;
    public TextArea messageArea;
    public Button sendBtn;

    private Thread messageThread;
    private ObservableList<String> messages = FXCollections.observableArrayList();

    public void initialize() {
        messageList.setItems(messages);
        messageThread = new Thread(() -> {
            boolean run = true;
            while (run) {
                try {
                    byte[] buffer = new byte[1024];
                    DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
                    ChatSystem.getInstance().getSocket().receive(packet);
                    String msg = new String(packet.getData());
                    String[] data = msg.split(":");
                    switch (data[0].trim()) {
                        case "CLIENT_JOIN":
                            Platform.runLater(() -> {
                                messages.add(data[1].trim() + " has joined the chat!");
                            });
                            break;
                        case "CHAT_MSG":
                            Platform.runLater(() -> {
                                messages.add(data[1].trim() + ": " + data[2].trim());
                            });
                            break;
                        case "CLIENT_DC":
                            Platform.runLater(() -> {
                                messages.add(data[1].trim() + " has left the chat!");
                            });
                            if(data[1].trim().equalsIgnoreCase(ChatSystem.getInstance().getUsername()))
                                run = false;
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        messageThread.start();
    }

    public void shutdown() {
        messageThread.interrupt();
    }

    public void onSendBtnAction(ActionEvent actionEvent) {
        ChatSystem.getInstance().sendMessage(messageArea.getText());
        messageArea.clear();
    }
}
