package ut3multicastchat.stages;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import ut3multicastchat.ChatSystem;
import ut3multicastchat.MainApp;

import java.io.IOException;

public class LoginStage {
    public TextField nameField;

    public void loginAction(ActionEvent actionEvent) {
        ChatSystem.getInstance().setUsername(nameField.getText().trim());
        try {
            FXMLLoader loader = new FXMLLoader(MainApp.class.getResource("stages/ChatStage.fxml"));
            Parent root = loader.load();
            ChatStage controller = loader.getController();
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setTitle("Chat");
            stage.setResizable(true);
            stage.setScene(scene);
            stage.setOnHidden(e -> controller.shutdown());
            stage.show();
            ((Stage)nameField.getScene().getWindow()).close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
