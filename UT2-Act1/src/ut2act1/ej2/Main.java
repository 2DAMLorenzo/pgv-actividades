package ut2act1.ej2;

/**
 * Created by thel0w3r on 24/10/2018.
 * All Rights Reserved.
 */
public class Main {

    public static void main(String[] args) {
        CustomThread t1 = new CustomThread("Thread 1");
        CustomThread t2 = new CustomThread("Thread 2");
        CustomThread t3 = new CustomThread("Thread 3");
        t1.setPriority(Thread.MIN_PRIORITY);
        t3.setPriority(Thread.MAX_PRIORITY);

        t1.start();
        t2.start();
        t3.start();

        try {
            t1.join();
            t2.join();
            t3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("ALL THREADS FINISHED");
    }

}
