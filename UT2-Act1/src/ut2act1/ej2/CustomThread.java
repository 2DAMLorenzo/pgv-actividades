package ut2act1.ej2;

/**
 * Created by thel0w3r on 24/10/2018.
 * All Rights Reserved.
 */
public class CustomThread extends Thread {

    public CustomThread(String name) {
        super(name);
    }

    @Override
    public void run() {
        for(int i = 0; i < 10; i++) {
            if(i == 4) {
                if(Thread.currentThread().getPriority() == Thread.MIN_PRIORITY) {
                    this.setPriority(Thread.MAX_PRIORITY);
                } else  {
                    this.setPriority(Thread.MIN_PRIORITY);
                }
            }
            System.out.println("Running on " + Thread.currentThread().getName() + " with priority " + this.getPriority() + ": " + (i+1));
            try {
                Thread.sleep((long)(Math.random() * 1000));
            } catch (InterruptedException e) {
                System.err.println("Thread " + Thread.currentThread().getName() + " interrupted!");
            }
        }
    }

}
