package ut2act1.ej1;

/**
 * Created by thel0w3r on 24/10/2018.
 * All Rights Reserved.
 */
public class CustomRunnable implements Runnable {
    @Override
    public void run() {
        for(int i = 0; i < 5; i++) {
            System.out.println("Running on " + Thread.currentThread().getName() + ": " + (i+1));
            try {
                Thread.sleep((long)(Math.random() * 600));
            } catch (InterruptedException e) {
                System.err.println("Thread " + Thread.currentThread().getName() + " interrupted!");
            }
        }
    }
}
