package ut2act1.ej1;

/**
 * Created by thel0w3r on 24/10/2018.
 * All Rights Reserved.
 */
public class Main {

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(new CustomRunnable(), "Thread 1");
        Thread t2 = new Thread(new CustomRunnable(), "Thread 2");
        Thread t3 = new Thread(new CustomRunnable(), "Thread 3");

        t1.start();
        t2.start();
        t3.start();

        try {
            t1.join();
            t2.join();
            t3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("ALL THREADS FINISHED");
    }

}
