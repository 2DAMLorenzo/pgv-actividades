package ut1act1;

import ut1act1.util.Window;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

/**
 * Created by TheL0w3R on 03/10/2018.
 * All Rights Reserved.
 */
public class MainWindow extends Window {

    private JButton calcBtn;
    private JButton regeditBtn;
    private JButton batBtn;
    private JButton customBtn;
    private JFileChooser batChooser;

    public static void main(String[] args) {
        new MainWindow();
    }

    public MainWindow() {
        super("Ejecución de procesos", 300, 200);
        setupLayout();
        setupEventHandlers();
        this.setResizable(false);
        this.setVisible(true);
    }

    private void setupLayout() {
        calcBtn = new JButton("Open Windows Calculator");
        position(0, calcBtn);
        regeditBtn = new JButton("Open Windows Registry");
        position(1, regeditBtn);
        batBtn = new JButton("Run batch file");
        position(2, batBtn);
        customBtn = new JButton("Run custom command");
        position(3, customBtn);
        batChooser = new JFileChooser();
    }

    private void setupEventHandlers() {
        calcBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                runWithException("calc.exe");
            }
        });
        regeditBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                runWithException("regedit.exe");
            }
        });
        batBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int returnVal = batChooser.showOpenDialog(MainWindow.this);
                if(returnVal == JFileChooser.APPROVE_OPTION) {
                    runWithException(batChooser.getSelectedFile().getAbsolutePath());
                }
            }
        });
        customBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String val = JOptionPane.showInputDialog(MainWindow.this, "Enter the command you want to run:", "Run custom command", JOptionPane.PLAIN_MESSAGE);
                if(val != null)
                    runWithException(val);
            }
        });
    }

    private void position(int y, JComponent c) {
        positionElement(0, y, 1, 1, 1, 0, c);
    }

    private void runWithException(String... command) {
        try {
            new ProcessBuilder(command).start();
        } catch (IOException e1) {
            JOptionPane.showMessageDialog(null, "An error has occurred trying to run the command.\r\n" + e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

}
