package ut1examen.parent;

import java.io.*;
import java.util.Scanner;

/**
 * Created by TheL0w3R on 18/10/2018.
 * All Rights Reserved.
 */
public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        try {
            Process toAscii = new ProcessBuilder("java", "-jar", "C:\\Users\\AlumnoDAM\\IdeaProjects\\ExamenLorenzo\\out\\artifacts\\ExamenLorenzo_jar\\child.jar").start();
            Process ping = new ProcessBuilder("cmd", "/C", "ping", "localhost").start();
            Process time = new ProcessBuilder("cmd", "/C", "time").start();

            BufferedWriter taW = new BufferedWriter(new OutputStreamWriter(toAscii.getOutputStream()));
            BufferedReader taR = new BufferedReader(new InputStreamReader(toAscii.getInputStream()));

            System.out.print("Indica el caracter para conocer su valor ASCII: ");
            taW.write(sc.nextLine().charAt(0));
            taW.newLine();
            taW.flush();
            taW.close();
            System.out.println("El valor ASCII es " + taR.readLine() + "\r\n");
            taR.close();

            System.out.println("Haciendo ping a localhost... Esperando respuesta...");
            BufferedReader pingR = new BufferedReader(new InputStreamReader(ping.getInputStream()));
            System.out.println(readFromStream(pingR) + "\r\n");
            pingR.close();

            BufferedWriter timeW = new BufferedWriter(new OutputStreamWriter(time.getOutputStream()));
            BufferedReader timeR = new BufferedReader(new InputStreamReader(time.getInputStream()));

            String timeLineOut;
            while ((timeLineOut = timeR.readLine()) != null) {
                if(timeLineOut.contains("La hora actual es:")) {
                    String timeData = timeLineOut.substring(18).trim();
                    String currentHour = timeData.substring(0, timeData.indexOf(":"));

                    System.out.println(timeLineOut.substring(0, timeLineOut.indexOf(",")));

                    String finalTime = timeData.replace(timeData.substring(0, timeData.indexOf(":")), String.valueOf(Integer.valueOf(currentHour) + 1));
                    System.out.println("Actualizando la hora a " + finalTime.substring(0, finalTime.indexOf(",")));
                    timeW.write(finalTime);
                    timeW.newLine();
                    timeW.flush();
                    timeW.close();
                }
            }

            System.out.println(readFromStream(timeR));
            timeR.close();

            while(toAscii.isAlive() || ping.isAlive() || time.isAlive()) {}
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String readFromStream(BufferedReader reader) throws IOException {
        StringBuilder sb = new StringBuilder();
        String tmp;
        while ((tmp = reader.readLine()) != null) {
            sb.append(tmp).append("\r\n");
        }
        return sb.toString();
    }

}
