package ut3chatserver;

import java.net.Socket;

public class Client {

    private ClientConnection clientConnection;
    private String name;

    public Client(ClientConnection clientConnection, String name) {
        this.clientConnection = clientConnection;
        this.name = name;
    }

    public Socket getSocket() {
        return clientConnection.getClientSocket();
    }

    public String getName() {
        return name;
    }
}
