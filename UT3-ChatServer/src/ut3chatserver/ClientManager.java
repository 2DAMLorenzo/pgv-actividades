package ut3chatserver;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ClientManager {

    private static ClientManager instance;
    private List<Client> clients;

    public ClientManager() {
        instance = this;
        clients = new ArrayList<>();
    }

    public static ClientManager getInstance() {
        return instance;
    }

    public List<Client> getClients() {
        return clients;
    }

    public Client getClient(Socket socket) {
        for(Client c : clients) {
            if(c.getSocket().equals(socket))
                return c;
        }
        return null;
    }

    public synchronized void removeClient(Socket sock) {
        clients.removeIf(client -> client.getSocket().equals(sock));
    }

    public synchronized boolean authenticate(Client client) {
        for(Client c : clients)
            if(c.getName().equalsIgnoreCase(client.getName()))
                return false;
        clients.add(client);
        return true;
    }

    public synchronized void broadcast(String data) throws IOException {
        for(Client client : clients) {
            if(client.getSocket().isConnected()) {
                BufferedWriter out = new BufferedWriter(new OutputStreamWriter(client.getSocket().getOutputStream()));
                out.write(data);
                out.newLine();
                out.flush();
            }
        }
    }

    public synchronized void broadcastExcept(String data, Socket sock) throws IOException {
        for(Client client : clients) {
            if(client.getSocket().isConnected() && !client.getSocket().equals(sock)) {
                BufferedWriter out = new BufferedWriter(new OutputStreamWriter(client.getSocket().getOutputStream()));
                out.write(data);
                out.newLine();
                out.flush();
            }
        }
    }
}
