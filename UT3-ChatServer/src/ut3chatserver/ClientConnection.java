package ut3chatserver;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.Socket;

public class ClientConnection extends Thread {

    private Socket clientSocket;

    public ClientConnection(Socket clientSocket) {
        this.clientSocket = clientSocket;
    }

    public Socket getClientSocket() {
        return clientSocket;
    }

    @Override
    public void run() {
        boolean run = true;
        try {
            InetAddress clientAddress = clientSocket.getInetAddress();
            System.out.println("Connecting client on " + clientAddress);
            BufferedReader inStream = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            BufferedWriter outStream = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
            while (run) {
                String[] data = inStream.readLine().split(":");
                switch (data[0].trim()) {
                    case "AUTH":
                        String username = data[1].trim();
                        if (ClientManager.getInstance().authenticate(new Client(this, username))) {
                            System.out.println("Client authenticated: " + username);
                            outStream.write("LOGIN_GRANTED");
                            outStream.newLine();
                            outStream.flush();
                            ClientManager.getInstance().broadcastExcept("CLIENT_JOIN:" + username, clientSocket);
                        } else {
                            outStream.write("LOGIN_DENIED");
                            outStream.newLine();
                            outStream.flush();
                        }
                        break;
                    case "MSG":
                        String message = data[1].trim();
                        ClientManager.getInstance().broadcast("CHAT_MSG:" +
                                ClientManager.getInstance().getClient(clientSocket).getName() + ":" + message);
                        break;
                    case "DC":
                        System.out.println("Client disconnecting: " + ClientManager.getInstance().getClient(clientSocket).getName());
                        ClientManager.getInstance().broadcast("CLIENT_DC:" +
                                ClientManager.getInstance().getClient(clientSocket).getName());
                        ClientManager.getInstance().removeClient(clientSocket);
                        clientSocket.close();
                        run = false;
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
