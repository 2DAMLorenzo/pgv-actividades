package ut3chatserver;

import java.io.*;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class Main {

    public static void main(String[] args) throws IOException {
        new ClientManager();
        ServerSocket ss = new ServerSocket();
        ss.bind(new InetSocketAddress(InetAddress.getLocalHost().getHostAddress(), 46369));
        Thread listenThread = new Thread(
            () -> {
                while (true) {
                    try {
                        new ClientConnection(ss.accept()).start();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        );
        listenThread.start();
    }

}
