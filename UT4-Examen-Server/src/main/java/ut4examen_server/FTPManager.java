package ut4examen_server;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.io.CopyStreamListener;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FTPManager {

    private static FTPManager instance;

    private FTPClient ftpClient;

    public FTPManager() {
        instance = this;
        ftpClient = new FTPClient();
        ftpClient.setConnectTimeout(5000);
        ftpClient.setControlEncoding("UTF-8");
    }

    public static FTPManager getInstance() {
        return instance;
    }

    private boolean connect(String host, int port) {
        try {
            ftpClient.connect(host, port);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean authenticate(String host, int port, String username, String password) {
        boolean connection = connect(host, port);

        if(!connection)
            return false;

        try {
            ftpClient.login(username, password);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    public synchronized String getCurrentDirectory() {
        try {
            return ftpClient.printWorkingDirectory();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public synchronized FTPFile[] getFiles() {
        try {
            return ftpClient.listFiles();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public synchronized long getFileSize(String filename) {
        try {
            return ftpClient.mlistFile(filename).getSize();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public synchronized boolean changeDirectory(String folder) {
        try {
            return ftpClient.changeWorkingDirectory(folder);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public synchronized void downloadFile(String remoteFileName, String localPath, CopyStreamListener progressListener) {
        ftpClient.setCopyStreamListener(progressListener);
        new Thread(() -> {
            try {
                FileOutputStream fout = new FileOutputStream(localPath);
                ftpClient.setFileType(FTP.BINARY_FILE_TYPE, FTP.BINARY_FILE_TYPE);
                ftpClient.setFileTransferMode(FTP.BINARY_FILE_TYPE);
                if(ftpClient.retrieveFile(remoteFileName, fout))
                    System.out.println("File downloaded successfully!");
                else
                    System.out.println("Error downloading file!");
                fout.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    public synchronized void uploadFile(String remoteFileName, String localPath, Callback callback) {
        System.out.println("Uploading " + localPath + " to " + remoteFileName);
        new Thread(() -> {
            File file = new File(localPath);

            if(!file.exists() || file.isDirectory())
                return;

            try {
                FileInputStream fis = new FileInputStream(file);
                ftpClient.setFileType(FTP.BINARY_FILE_TYPE, FTP.BINARY_FILE_TYPE);
                ftpClient.setFileTransferMode(FTP.BINARY_FILE_TYPE);
                if(ftpClient.storeFile(remoteFileName, fis)) {
                    System.out.println("File " + file.getName() + " uploaded successfully!");
                    callback.callback();
                } else
                    System.out.println("Error uploading file!");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    public synchronized void uploadFolder(String foldername, String localPath, Callback callback) {
        System.out.println("Uploading " + localPath + " to " + foldername);
        new Thread(() -> {
            File folder = new File(localPath);

            if(!folder.exists() || folder.isFile())
                return;

            try {
                ftpClient.makeDirectory(foldername);
                String currentDir = ftpClient.printWorkingDirectory();
                ftpClient.changeWorkingDirectory(foldername);

                for(File file : folder.listFiles()) {
                    if(file.isFile()) {
                        FileInputStream fis = new FileInputStream(file);
                        ftpClient.setFileType(FTP.BINARY_FILE_TYPE, FTP.BINARY_FILE_TYPE);
                        ftpClient.setFileTransferMode(FTP.BINARY_FILE_TYPE);
                        if(ftpClient.storeFile(file.getName(), fis))
                            System.out.println("File " + folder.getName() + " uploaded successfully!");
                        else
                            System.out.println("Error uploading file!");
                    }
                }
                callback.callback();
                ftpClient.changeWorkingDirectory(currentDir);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    public interface Callback {
        void callback();
    }
}
