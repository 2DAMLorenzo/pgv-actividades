package ut4examen_server;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

public class ClientManager {

    private List<Client> clients;

    public ClientManager() {
        clients = new ArrayList<>();
    }

    public synchronized boolean addClient(Client client) {
        for(Client c : clients) {
            if(c.getName().equalsIgnoreCase(client.getName()))
                return false;
        }

        clients.add(client);
        return true;
    }

    public synchronized Client[] getClients() {
        return clients.toArray(new Client[0]);
    }

    public synchronized Client getByName(String name) {
        for(Client client : clients) {
            if(client.getName().equalsIgnoreCase(name))
                return client;
        }

        return null;
    }

    public synchronized void removeClient(Client client) {
        clients.remove(client);
    }
}
