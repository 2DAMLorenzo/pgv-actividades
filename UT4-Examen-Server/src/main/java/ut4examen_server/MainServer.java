package ut4examen_server;

import org.apache.commons.net.ftp.FTPFile;

import java.io.File;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class MainServer {

    private static final int PORT = 6667;
    private static final File HOME_DIR = new File("/Users/thel0w3r/Documents/ExamenPGV");

    private static ClientManager clientManager = new ClientManager();

    public static void main(String[] args) {
        new FTPManager().authenticate("172.16.165.130", 21, "ExamenLorenzo", "root");
        try {
            DatagramSocket socket = new DatagramSocket(PORT);
            System.out.println("Server started. Listening on port " + PORT);
            while (true) {
                System.out.println("Waiting for requests...");
                try {
                    byte[] data = new byte[2048];
                    DatagramPacket packet = new DatagramPacket(data, data.length);
                    socket.receive(packet);
                    new Thread(() -> {
                        InetAddress incomingAddress = packet.getAddress();
                        int incomingPort = packet.getPort();
                        String message = new String(packet.getData());
                        String[] parsed = message.split(":");
                        switch(parsed[0].trim()) {
                            case "AUTH":
                                System.out.println("Authenticating client on " + incomingAddress.toString() +
                                        " with name " + parsed[1].trim());
                                if(clientManager.addClient(new Client(parsed[1].trim(), incomingAddress))) {
                                    sendResponse(socket, "AUTH_GRANTED", incomingAddress, incomingPort);
                                    System.out.println("Client authenticated successfully!");
                                } else {
                                    sendResponse(socket, "AUTH_DENIED", incomingAddress, incomingPort);
                                    System.out.println("A client with the name " + parsed[1].trim() + " is already logged in!");
                                }
                                break;
                            case "UPLOAD":
                                File wantedFile = new File(parsed[1].trim());
                                if(wantedFile.isDirectory()) {
                                    FTPManager.getInstance().uploadFolder(wantedFile.getName(), wantedFile.getAbsolutePath(),
                                            () -> sendResponse(socket, "UPLOAD_SUCCESSFUL:" +
                                                    wantedFile.getName(), incomingAddress, incomingPort));
                                    return;
                                }
                                FTPManager.getInstance().uploadFile(wantedFile.getName(), wantedFile.getAbsolutePath(),
                                        () -> sendResponse(socket, "UPLOAD_SUCCESSFUL:" +
                                                wantedFile.getName(), incomingAddress, incomingPort));
                                break;
                            case "COM":
                                runFileCount(socket, incomingAddress, incomingPort, parsed[1]);
                            break;
                            case "DC":
                                disconnectClient(parsed[1]);
                            break;
                            default:
                                System.out.println("Unknown request!");
                                sendResponse(socket, "UNKNOWN_REQUEST", incomingAddress, incomingPort);
                                break;
                        }
                    }).start();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    public static void runFileCount(DatagramSocket socket, InetAddress incomingAddress, int incomingPort, String s) {
        int fileCount = 0;
        Client client = clientManager.getByName(s.trim());

        if(client == null) {
            System.out.println("Unauthorized client!");
            return;
        }

        System.out.println("Listing files for " + client.getName());

        fileCount = countFiles("/");

        System.out.println("Sending response with the files/directories count");
        sendResponse(socket, "COUNT:" + fileCount, incomingAddress, incomingPort);
        FTPManager.getInstance().changeDirectory("/");
    }

    public static void disconnectClient(String s) {
        Client client = clientManager.getByName(s.trim());
        if(client == null)
            return;
        System.out.println("Disconnecting client " + client.getName());
        clientManager.removeClient(client);
    }

    private static void sendResponse(DatagramSocket socket, String data, InetAddress address, int port) {
        byte[] responseData = data.getBytes();
        DatagramPacket response = new DatagramPacket(responseData, responseData.length,
                address, port);
        try {
            socket.send(response);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static int countFiles(String folder) {
        int count = 0;
        System.out.println(folder + ":");
        String currentDir = FTPManager.getInstance().getCurrentDirectory();
        FTPManager.getInstance().changeDirectory(folder);
        for(FTPFile file : FTPManager.getInstance().getFiles()) {
            count++;
            if(file.isDirectory()) {
                count += countFiles(file.getName());
            } else
                System.out.println("  - " + file.getName());
        }
        FTPManager.getInstance().changeDirectory(currentDir);
        return count;
    }

}
