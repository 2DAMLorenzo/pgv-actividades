package ut2act2;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by TheL0w3R on 12/11/2018.
 * All Rights Reserved.
 */
public class ExerciseManager {

    private static ExerciseManager instance;

    private ArrayList<Exercise> exercises;

    public ExerciseManager(Exercise... exs) {
        instance = this;
        exercises = new ArrayList<>(Arrays.asList(exs));
    }

    public static ExerciseManager getInstance() {
        return instance;
    }

    public Exercise getExercise(String name) {
        Exercise ex = null;
        for(Exercise e : exercises) {
            if(e.getName().equalsIgnoreCase(name))
                ex = e;
        }
        return ex;
    }

    public boolean areExercisesRunning() {
        for(Exercise e : exercises)
            if(e.isRunning())
                return true;
        return false;
    }

}
