package ut2act2;

import java.util.ArrayList;

/**
 * Created by TheL0w3R on 12/11/2018.
 * All Rights Reserved.
 */
public abstract class Exercise {

    private boolean running;
    private String name;
    protected ArrayList<Thread> threads;

    public Exercise(String name) {
        running = false;
        threads = new ArrayList<>();
        this.name = name;
    }

    public abstract void run();

    public boolean isRunning() {
        return running;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void runAllThreads() {
        for(Thread t : threads)
            t.start();

        this.running = true;
    }

    public void stopAllThreads() {
        for(Thread t : threads)
            t.stop();

        this.running = false;
    }
}
