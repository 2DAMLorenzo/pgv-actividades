package ut2act2;

import ut2act2.util.AppendableTextPane;
import ut2act2.util.Window;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by TheL0w3R on 12/11/2018.
 * All Rights Reserved.
 */
public class MainWindow extends Window {

    private static MainWindow instance;

    private JButton runProductConsumerButton, runParkingButton, stopButton, clearButton;
    private AppendableTextPane outputTextPane;

    public MainWindow() {
        super("Actividad 2 - UT2 [Lorenzo Pinna Rodríguez]");
        instance = this;
        setupLayout();
        setupEventListeners();
        this.setVisible(true);
    }

    public static MainWindow getInstance() {
        return instance;
    }

    private void setupLayout() {
        runProductConsumerButton = new JButton("Run P/C Threads");
        runParkingButton = new JButton("Run ParkingExercise Threads");
        clearButton = new JButton("Clear output");
        stopButton = new JButton("Stop all Threads");
        outputTextPane = new AppendableTextPane();
        positionElement(0, 0, 1, 1, 1, 0, runProductConsumerButton);
        positionElement(1, 0, 1, 1, 1, 0, new Insets(5, 0, 5, 5), GridBagConstraints.BOTH, runParkingButton);
        positionElement(0, 1, 2, 1, 1, 1, new Insets(0, 5, 0, 5), GridBagConstraints.BOTH, new JScrollPane(outputTextPane));
        positionElement(0, 2, 1, 1, 1, 0, clearButton);
        positionElement(1, 2, 1, 1, 1, 0, new Insets(5, 0, 5, 5), GridBagConstraints.BOTH, stopButton);
    }

    private void setupEventListeners() {
        runProductConsumerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!ExerciseManager.getInstance().areExercisesRunning()) {
                    appendText("<b>**** STARTING \"Product Consumer\" EXERCISE ****</b>");
                    ExerciseManager.getInstance().getExercise("ProductConsumerExercise").run();
                } else {
                    JOptionPane.showMessageDialog(MainWindow.this, "There is an exercise running!\nPlease, stop all threads before running a new exercise.", "Exercise already running!", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        runParkingButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!ExerciseManager.getInstance().areExercisesRunning()) {
                    appendText("<b>**** STARTING \"ParkingExercise\" EXERCISE ****</b>");
                    ExerciseManager.getInstance().getExercise("ParkingExercise").run();
                } else {
                    JOptionPane.showMessageDialog(MainWindow.this, "There is an exercise running!\nPlease, stop all threads before running a new exercise.", "Exercise already running!", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        stopButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ExerciseManager.getInstance().getExercise("ProductConsumerExercise").stopAllThreads();
                ExerciseManager.getInstance().getExercise("ParkingExercise").stopAllThreads();
                appendText("<b>**** ALL THREADS STOPPED ****</b>");
            }
        });
        clearButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                outputTextPane.setText("");
            }
        });
    }

    public void appendText(String text) {
        outputTextPane.appendText(text);
    }
}
