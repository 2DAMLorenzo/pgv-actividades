package ut2act2;

import ut2act2.exercises.parking.ParkingExercise;
import ut2act2.exercises.productconsumer.ProductConsumerExercise;

/**
 * Created by thel0w3r on 12/11/2018.
 * All Rights Reserved.
 */
public class Main {

    public static void main(String[] args) {
        new ExerciseManager(new ProductConsumerExercise(), new ParkingExercise());
        new MainWindow();
    }

}
