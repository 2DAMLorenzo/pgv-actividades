package ut2act2.util;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import java.io.IOException;

/**
 * Created by TheL0w3R on 12/11/2018.
 * All Rights Reserved.
 */
public class AppendableTextPane extends JTextPane {

    public AppendableTextPane() {
        this.setContentType("text/html");
        this.setEditable(false);
    }

    public void appendText(String text) {
        HTMLDocument doc=(HTMLDocument) this.getStyledDocument();
        HTMLEditorKit kit = (HTMLEditorKit) this.getEditorKit();
        try {
            kit.insertHTML(doc, doc.getLength(), text, 0, 0, null);
            this.setCaretPosition(this.getDocument().getLength());
        } catch (BadLocationException | IOException e) {
            e.printStackTrace();
        }
    }
}
