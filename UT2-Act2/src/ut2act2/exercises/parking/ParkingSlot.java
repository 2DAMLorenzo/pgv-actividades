package ut2act2.exercises.parking;

/**
 * Created by TheL0w3R on 12/11/2018.
 * All Rights Reserved.
 */
public class ParkingSlot {

    private boolean isAvailable;
    private String carID;

    public ParkingSlot() {
        isAvailable = true;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public String getCarID() {
        return carID;
    }

    public void setCarID(String carID) {
        this.carID = carID;
    }
}
