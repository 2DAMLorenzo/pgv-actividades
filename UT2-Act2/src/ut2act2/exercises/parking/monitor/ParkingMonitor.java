package ut2act2.exercises.parking.monitor;

import ut2act2.MainWindow;
import ut2act2.exercises.parking.ParkingSlot;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by TheL0w3R on 12/11/2018.
 * All Rights Reserved.
 */
public class ParkingMonitor {

    private ParkingSlot[] parkingSlots;

    private static ParkingMonitor instance;

    public ParkingMonitor() {
        instance = this;
        parkingSlots = new ParkingSlot[25];
        for(int i = 0; i < 25; i++) {
            parkingSlots[i] = new ParkingSlot();
        }
    }

    public static ParkingMonitor getInstance() {
        return instance;
    }

    public synchronized boolean isParkingFull() {
        for(ParkingSlot slot : parkingSlots)
            if(slot.isAvailable())
                return false;
        return true;
    }

    public synchronized boolean park(String carID) throws InterruptedException {
        ArrayList<Integer> blacklist = new ArrayList<>();
        while(isParkingFull())
            wait();

        do {
            int randomIndex = new Random().nextInt(parkingSlots.length);
            if (!blacklist.contains(randomIndex)) {
                if (parkingSlots[randomIndex].isAvailable()) {
                    parkingSlots[randomIndex].setAvailable(false);
                    parkingSlots[randomIndex].setCarID(carID);
                    MainWindow.getInstance().appendText("<b style=\"color: #0B6E4F\">" + "[PARKING] Car " + carID + " has been parked at " + randomIndex + "</b>");
                    return true;
                } else {
                    blacklist.add(randomIndex);
                }
            }

        } while (blacklist.size() < 25);

        return false;
    }

    public synchronized void freeSlot(String carID) throws InterruptedException {
        boolean slotCleared = false;
        for(int i = 0; i < parkingSlots.length; i++) {
            ParkingSlot slot = parkingSlots[i];
            if(slot.getCarID() != null && slot.getCarID().equalsIgnoreCase(carID)) {
                slot.setAvailable(true);
                slot.setCarID(null);
                slotCleared = true;
                MainWindow.getInstance().appendText("<b style=\"color: #DE541E\">" + "[PARKING] Car " + carID + " has left his parking slot with ID " + i + "</b>");
                notifyAll();
            }
        }
        if(!slotCleared)
            wait();
    }
}
