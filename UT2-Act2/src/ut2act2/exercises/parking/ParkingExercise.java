package ut2act2.exercises.parking;

import ut2act2.Exercise;
import ut2act2.exercises.parking.monitor.Car;
import ut2act2.exercises.parking.monitor.ParkingMonitor;
import ut2act2.exercises.parking.semaphore.Parking;

import javax.swing.*;
import java.awt.*;

/**
 * Created by TheL0w3R on 12/11/2018.
 * All Rights Reserved.
 */
public class ParkingExercise extends Exercise {

    public ParkingExercise() {
        super("ParkingExercise");
    }

    @Override
    public void run() {
        JPanel panel = new JPanel(new GridLayout(0, 1));
        JComboBox<String> mode = new JComboBox<>();
        mode.addItem("Monitor");
        mode.addItem("Semaphore");
        panel.add(new JLabel("Select a method for thread synchronization:"));
        panel.add(mode);
        int result = JOptionPane.showConfirmDialog(null, panel, "Select thread synchronization method",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (result == JOptionPane.OK_OPTION) {
            if(mode.getSelectedItem() == "Monitor")
                runWithMonitor();
            else
                runWithSemaphore();
        }
    }

    private void runWithMonitor() {
        new ParkingMonitor();
        this.threads.clear();
        for(int i = 0; i < 50; i++)
            this.threads.add(new Car("AmazingCar " + (i+1)));

        runAllThreads();
    }

    private void runWithSemaphore() {
        new Parking();
        this.threads.clear();
        for(int i = 0; i < 50; i++)
            this.threads.add(new ut2act2.exercises.parking.semaphore.Car("SemaphoreKhar " + (i+1)));

        runAllThreads();
    }
}
