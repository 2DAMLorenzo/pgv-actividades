package ut2act2.exercises.parking.semaphore;

/**
 * Created by TheL0w3R on 28/11/2018.
 * All Rights Reserved.
 */
public class Car extends Thread {

    private boolean isParked;

    public Car(String name) {
        isParked = false;
        this.setName(name);
    }

    @Override
    public void run() {
        while(true) {
            try {
                if(!isParked)
                    isParked = Parking.getInstance().park(currentThread().getName());
                else {
                    Thread.sleep((long)(10000 * Math.random()));
                    Parking.getInstance().freeSlot(currentThread().getName());
                    isParked = false;
                }

                Thread.sleep((long)(5000 * Math.random()));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
