package ut2act2.exercises.parking.semaphore;

import ut2act2.MainWindow;
import ut2act2.exercises.parking.ParkingSlot;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Semaphore;

/**
 * Created by TheL0w3R on 28/11/2018.
 * All Rights Reserved.
 */
public class Parking {

    private ParkingSlot[] parkingSlots;
    private Semaphore mutex;

    private static Parking instance;

    public Parking() {
        instance = this;
        mutex = new Semaphore(1);
        parkingSlots = new ParkingSlot[25];
        for(int i = 0; i < 25; i++) {
            parkingSlots[i] = new ParkingSlot();
        }
    }

    public static Parking getInstance() {
        return instance;
    }

    public boolean isParkingFull() {
        for(ParkingSlot slot : parkingSlots)
            if(slot.isAvailable())
                return false;
        return true;
    }

    public boolean park(String carID) throws InterruptedException {
        mutex.acquire();

        ArrayList<Integer> blacklist = new ArrayList<>();
        if(isParkingFull()) {
            mutex.release();
            return false;
        }

        do {
            int randomIndex = new Random().nextInt(parkingSlots.length);
            if (!blacklist.contains(randomIndex)) {
                if (parkingSlots[randomIndex].isAvailable()) {
                    parkingSlots[randomIndex].setAvailable(false);
                    parkingSlots[randomIndex].setCarID(carID);
                    MainWindow.getInstance().appendText("<b style=\"color: #0B6E4F\">" + "[PARKING] Car " + carID + " has been parked at " + randomIndex + "</b>");
                    mutex.release();
                    return true;
                } else {
                    blacklist.add(randomIndex);
                }
            }

        } while (blacklist.size() < 25);

        mutex.release();

        return false;
    }

    public void freeSlot(String carID) throws InterruptedException {
        mutex.acquire();
        boolean slotCleared = false;
        for(int i = 0; i < parkingSlots.length; i++) {
            ParkingSlot slot = parkingSlots[i];
            if(slot.getCarID() != null && slot.getCarID().equalsIgnoreCase(carID)) {
                slot.setAvailable(true);
                slot.setCarID(null);
                slotCleared = true;
                MainWindow.getInstance().appendText("<b style=\"color: #DE541E\">" + "[PARKING] Car " + carID + " has left his parking slot with ID " + i + "</b>");
            }
        }
        if(!slotCleared)
            wait();
        mutex.release();
    }

}
