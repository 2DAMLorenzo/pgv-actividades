package ut2act2.exercises.productconsumer;

/**
 * Created by thel0w3r on 12/11/2018.
 * All Rights Reserved.
 */
public class Consumer extends Thread {

    public Consumer(String name) {
        this.setName(name);
    }

    @Override
    public void run() {
        while(true) {
            try {
                PCMonitor.getInstance().consume(currentThread().getName());
                Thread.sleep((long)(2000 * Math.random()));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
