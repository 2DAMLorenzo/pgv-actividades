package ut2act2.exercises.productconsumer;

import ut2act2.MainWindow;

/**
 * Created by thel0w3r on 12/11/2018.
 * All Rights Reserved.
 */
public class PCMonitor {

    private static PCMonitor instance;

    private double value;

    public PCMonitor() {
        instance = this;
        value = 0;
    }

    public static PCMonitor getInstance() {
        return instance;
    }

    public synchronized void produce(double val, String name) throws InterruptedException {
        while(value != 0) {
            wait();
        }

        value = val;
        MainWindow.getInstance().appendText("<b style=\"color: #BF211E\">" + "[PRODUCING] " + name + " has produced " + val + "</b>");
        notifyAll();
    }

    public synchronized void consume(String name) throws InterruptedException {
        while(value == 0) {
            wait();
        }
        double val = value;
        value = 0;
        MainWindow.getInstance().appendText("<b style=\"color: #69A197\">" + "[CONSUMING] " + name + " has consumed " + val + "</b>");
        notifyAll();
    }

}
