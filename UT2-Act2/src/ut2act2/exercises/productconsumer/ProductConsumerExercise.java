package ut2act2.exercises.productconsumer;

import ut2act2.Exercise;

import java.util.Random;

/**
 * Created by TheL0w3R on 12/11/2018.
 * All Rights Reserved.
 */
public class ProductConsumerExercise extends Exercise {

    public ProductConsumerExercise() {
        super("ProductConsumerExercise");
    }

    @Override
    public void run() {
        new PCMonitor();
        this.threads.clear();
        for(int i = 0; i < 10; i++) {
            boolean isConsumer = new Random().nextInt(2) == 1;
            if(isConsumer)
                this.threads.add(new Consumer("Consumer " + (i+1)));
            else
                this.threads.add(new Producer("Producer " + (i+1)));
        }

        runAllThreads();
    }
}
