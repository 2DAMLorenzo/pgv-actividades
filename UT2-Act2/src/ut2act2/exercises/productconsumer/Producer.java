package ut2act2.exercises.productconsumer;

import java.util.Random;

/**
 * Created by thel0w3r on 12/11/2018.
 * All Rights Reserved.
 */
public class Producer extends Thread {

    public Producer(String name) {
        this.setName(name);
    }

    @Override
    public void run() {
        while(true) {
            double val = new Random().nextInt(10) + 1;
            try {
                PCMonitor.getInstance().produce(val, currentThread().getName());
                Thread.sleep((long)(2300 * Math.random()));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
