package ut2repaso;

import java.util.ArrayList;

/**
 * Created by thel0w3r on 19/11/2018.
 * All Rights Reserved.
 */
public class Monitor {

    private static Monitor instance;

    private ArrayList<Stick> stickPool;

    public Monitor() {
        instance = this;
        stickPool = new ArrayList<>();
        for(int i = 0; i < 5; i++)
            stickPool.add(new Stick());
    }

    public static Monitor getInstance() {
        return instance;
    }

    public synchronized boolean askForEating(int philosopherID) {
        try {
            while(!philosopherHasSticksFree(philosopherID)) {
                wait();
            }

            for(Stick correspondingStick : getPhilosopherSticks(philosopherID))
                correspondingStick.setInUse(true);

            System.out.println("\nPhilosopher " + philosopherID + " has started eating");
            System.out.println("STICK STATUS:");
            for(int i = 0; i < stickPool.size(); i++)
                System.out.println("    " + i + ": " + stickPool.get(i).isInUse());
            return true;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }

    public synchronized void notifyStopEating(int philosopherID) {
        for(Stick correspondingStick : getPhilosopherSticks(philosopherID))
            correspondingStick.setInUse(false);

        System.out.println("\nPhilosopher " + philosopherID + " has STOPPED eating");
        notifyAll();
    }

    private synchronized boolean philosopherHasSticksFree(int philosopherID) {
        Stick[] sticks = getPhilosopherSticks(philosopherID);
        return (!sticks[0].isInUse()) && (!sticks[1].isInUse());
    }

    private synchronized Stick[] getPhilosopherSticks(int philosopherID) {
        Stick[] sticks = new Stick[2];
        boolean cycling = philosopherID == (stickPool.size() - 1);
        sticks[0] = stickPool.get(philosopherID);
        sticks[1] = stickPool.get(cycling ? 0 : (philosopherID + 1));
        return sticks;
    }
}
