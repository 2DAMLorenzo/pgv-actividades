package ut2repaso;

/**
 * Created by thel0w3r on 19/11/2018.
 * All Rights Reserved.
 */
public class Philosopher extends Thread {

    private int id;
    private boolean isEating;

    public Philosopher(int id) {
        this.id = id;
        isEating = false;
    }

    private void tryEating() {
        isEating = Monitor.getInstance().askForEating(id);
    }

    private void tryThinking() {
        Monitor.getInstance().notifyStopEating(id);
        isEating = false;
    }

    @Override
    public void run() {
        while(true) {
            try {
                if(!isEating)
                    tryEating();
                else
                    tryThinking();

                Thread.sleep((long) (2000 * Math.random()));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
