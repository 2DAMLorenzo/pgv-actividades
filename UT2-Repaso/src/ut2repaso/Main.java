package ut2repaso;

/**
 * Created by thel0w3r on 19/11/2018.
 * All Rights Reserved.
 */
public class Main {

    public static void main(String[] args) {
        new Monitor();
        for(int i = 0; i < 5; i++) {
            new Philosopher(i).start();
        }
    }

}
