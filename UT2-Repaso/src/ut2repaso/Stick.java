package ut2repaso;

/**
 * Created by thel0w3r on 19/11/2018.
 * All Rights Reserved.
 */
public class Stick {

    private boolean inUse;

    public Stick() {
        inUse = false;
    }

    public boolean isInUse() {
        return inUse;
    }

    public void setInUse(boolean inUse) {
        this.inUse = inUse;
    }
}
