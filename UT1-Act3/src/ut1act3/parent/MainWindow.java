package ut1act3.parent;

import ut1act3.parent.util.Window;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

/**
 * Created by TheL0w3R on 17/10/2018.
 * All Rights Reserved.
 */
public class MainWindow extends Window {

    public MainWindow() {
        super("Actividad 3 UT1");

        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new FileNameExtensionFilter("JAR files", "jar"));
        fileChooser.setAcceptAllFileFilterUsed(false);

        JButton startBtn = new JButton("Start scan");
        positionElement(0, 0, 1, 1, 1, 0, startBtn);
        JButton runJarFile = new JButton("Run JAR file");
        positionElement(1, 0, 1, 1, 1, 0, runJarFile);
        JLabel title = new JLabel("Services found:");
        positionElement(0, 1, 2, 1, 1, 0, title);
        JTextArea outputTA = new JTextArea();
        outputTA.setEditable(false);
        positionElement(0, 2, 2, 1, 1, 1, new JScrollPane(outputTA));
        JButton clearBtn = new JButton("Clear");
        positionElement(0, 3, 2, 1, 1, 0, clearBtn);

        startBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                outputTA.setText(getSservices());
            }
        });

        runJarFile.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int returnValue = fileChooser.showOpenDialog(MainWindow.this);
                if(returnValue == JFileChooser.APPROVE_OPTION) {
                    try {
                        Process proc = new ProcessBuilder("java", "-jar", fileChooser.getSelectedFile().getAbsolutePath()).start();
                        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(proc.getOutputStream()));
                        BufferedReader br = new BufferedReader(new InputStreamReader(proc.getInputStream()));
                        String serviceName = JOptionPane.showInputDialog("Enter the service name:");
                        if(serviceName != null) {
                            bw.write(serviceName);
                            bw.newLine();
                            bw.flush();
                            StringBuilder sb = new StringBuilder();
                            String line;
                            while((line = br.readLine()) != null)
                                sb.append(line).append("\n");

                            outputTA.setText(sb.toString());
                            br.close();
                            bw.close();
                        }
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });

        clearBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                outputTA.setText("");
                fileChooser.setSelectedFile(null);
            }
        });

        this.setVisible(true);
    }

    public static void main(String[] args) {
        new MainWindow();
    }

    private String getSservices() {
        StringBuilder sb = new StringBuilder();
        try {
            Process proc = new ProcessBuilder("tasklist", "/SVC", "/FO", "LIST").start();
            BufferedReader br = new BufferedReader(new InputStreamReader(proc.getInputStream()));
            String line;
            boolean readServices = false;
            int num = 1;
            while((line = br.readLine()) != null) {
                if(line.contains("svchost.exe")) {
                    sb.append("Servicios para el ").append(num).append("º \"svchost.exe\":").append("\n");
                    num++;
                    readServices = true;
                }
                if(readServices) {
                    if(line.contains("Servicios:")) {
                        String service = line.substring(10).trim();
                        if(service.equals("N/D"))
                            readServices = false;
                        else
                            sb.append(service).append("\n");
                    }
                }
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

}
