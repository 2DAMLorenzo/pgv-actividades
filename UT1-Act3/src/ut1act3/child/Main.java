package ut1act3.child;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 * Created by TheL0w3R on 17/10/2018.
 * All Rights Reserved.
 */
public class Main {

    public static void main(String[] args) {
        String service = new Scanner(System.in).nextLine();
        try {
            Process proc = new ProcessBuilder("sc", "qc", service).start();
            StringBuilder sb = new StringBuilder();
            BufferedReader br = new BufferedReader(new InputStreamReader(proc.getInputStream()));
            String line;
            while((line = br.readLine()) != null)
                sb.append(line).append("\n");

            System.out.println(sb.toString());
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
